To run the project follow the steps:

Build the environment:
```bash
virtual env -p python3 venv
source venv/bin/activate
pip install -r requirements.txt

python manage.py migrate
```

TO process the Apple Store CSV:

```bash
python manage.py process_csv csv AppleStore.csv
```

To find the most rated 'News' app:

```bash
python manage.py most_rating_news
```

To find the top 10 most rated 'Music' and 'Book' app:

```bash
python manage.py top_ten
```