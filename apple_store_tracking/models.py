from django.db import models


class AppCsvRegister(models.Model):
    track_name = models.CharField(max_length=500)
    n_citacoes = models.IntegerField(default=0)
    size_bytes = models.IntegerField(default=0)
    price = models.DecimalField(decimal_places=2, max_digits=10, default=0.0)
    prime_genre = models.CharField(max_length=100)
    rating_count_tot = models.IntegerField(default=0)

