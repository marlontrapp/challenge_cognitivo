from django.apps import AppConfig


class AppleStoreTrackingConfig(AppConfig):
    name = 'apple_store_tracking'
