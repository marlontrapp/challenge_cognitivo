import csv
from django.core.management.base import BaseCommand, CommandError
from apple_store_tracking.models import AppCsvRegister


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        new = AppCsvRegister.objects.filter(prime_genre="News").order_by('-rating_count_tot').first()
        all_fields = [column.name for column in AppCsvRegister._meta.get_fields()]
        for field in all_fields:
            print(f"{field}: {getattr(new, field)}")
