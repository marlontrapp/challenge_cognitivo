import csv
from django.core.management.base import BaseCommand, CommandError
from apple_store_tracking.models import AppCsvRegister


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('csv', nargs="+", type=str)

    def handle(self, *args, **options):
        all_fields = [column.name for column in AppCsvRegister._meta.get_fields()]
        with open(options['csv'][1], newline='\n') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                AppCsvRegister.objects.update_or_create(
                    **{key: value for key, value in row.items() if key in all_fields})
