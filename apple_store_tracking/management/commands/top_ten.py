import csv
from django.core.management.base import BaseCommand, CommandError
from apple_store_tracking.models import AppCsvRegister
import tweepy


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        print("#"*5 + " TOP TEN MUSIC " + "#"*5)
        musics = AppCsvRegister.objects.filter(prime_genre="Music").order_by('-rating_count_tot').all()[:10]
        all_fields = [column.name for column in AppCsvRegister._meta.get_fields()]
        for music in musics:
            for field in all_fields:
                print(f"{field}: {getattr(music, field)}")
            print("-"*20)
        print("#"*20)

        print("#"*5 + " TOP TEN MUSIC " + "#"*5)
        books = AppCsvRegister.objects.filter(prime_genre="Book").order_by('-rating_count_tot').all()[:10]
        all_fields = [column.name for column in AppCsvRegister._meta.get_fields()]
        for book in books:
            for field in all_fields:
                print(f"{field}: {getattr(book, field)}")
            print("-"*20)
        print("#"*20)
