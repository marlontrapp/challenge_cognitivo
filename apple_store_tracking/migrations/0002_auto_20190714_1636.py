# Generated by Django 2.2.1 on 2019-07-14 16:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apple_store_tracking', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appcsvregister',
            name='n_citacoes',
            field=models.IntegerField(null=True),
        ),
    ]
